package api

import (
	v1 "github/FIrstService/template-service/product-service/api-gateway/api/handlers/v1"
	"github/FIrstService/template-service/product-service/api-gateway/config"
	"github/FIrstService/template-service/product-service/api-gateway/pkg/logger"
	"github/FIrstService/template-service/product-service/api-gateway/services"

	_ "github/FIrstService/template-service/product-service/api-gateway/api/docs"

	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
}

// @title Swagger product API
// @version 1.0
// @description This is a product service.
// @termsOfService http://swagger.io/terms/

// @contact.name Azam
// @contact.url http://www.swagger.io/support
// @contact.email alievazam.200@swagger.io

// @BasePath

func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
	})
	// ProductService

	api := router.Group("/v1")
	api.POST("/products", handlerV1.CreateProduct)
	api.POST("/category", handlerV1.CreateCategory)
	api.POST("/type", handlerV1.CreateType)
	api.GET("/products/:id", handlerV1.GetProductinfobyID)
	api.DELETE("/delete/:id", handlerV1.DeleteInfo)
	api.POST("/update/:id", handlerV1.UpdateByID)
	//  Storeservice

	api.POST("/stores", handlerV1.CreateStore)
	api.GET("/stores/:id", handlerV1.GetStore)

	// Userservice

	api.POST("/users", handlerV1.CreateUser)

	// Swagger
	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler, url))

	return router

}
