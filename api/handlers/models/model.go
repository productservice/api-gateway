package models

type ProductFullInfo struct {
	Name       string
	Model      string
	TypeId     int
	CategoryId int
	Price      float64
	Amount     int
	Stores     []Store
}

type Product struct {
	Id         int
	Name       string
	Model      string
	TypeId     int64
	CategoryId int64
	Price      float32
	Amount     int
}

type Category struct {
	Name string
}

type Type struct {
	Name string
}

type Store struct {
	Name      string
	Addresses []Address
}

type Address struct {
	District string
	Street   string
	// storeid  int
}

// type StoreAddresses struct {
// 	storeid   int
// 	productid int
// }

type ProductFullInfoResponse struct {
	ID         int64
	Name       string
	Model      string
	TypeId     int
	CategoryId int
	Price      float64
	Amount     int
	Stores     []Store
}

type Ids struct {
	Id int64
}

type StoreRequest struct {
	Name    string
	Address []Address
}

type StoreResponse struct {
	Id           int64
	Name         string
	AddressResps []AddressResp
}

type GetstoreInfobyid struct {
	Id int64
}

type AddressResp struct {
	Id       int
	District string
	Street   string
}
