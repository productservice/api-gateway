package v1

import (
	"context"
	"github/FIrstService/template-service/product-service/api-gateway/genproto/user"
	l "github/FIrstService/template-service/product-service/api-gateway/pkg/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create User
// @Summary      Create User
// @Description  Creates new users
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        user   body user.User     true  "User"
// @Success      200  {object}  user.User
// @Router       /v1/users [post]
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body        user.User
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.UserService().Create(ctx, &user.User{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create product", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// func (h *handlerV1) Register(c *gin.Context) {
// 	var (
// 		body user.User
// 	)

// 	err := c.ShouldBindJSON(&body)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("error while bind json", l.Error(err))
// 		return
// 	}
// 	body.Email = strings.TrimSpace(body.Email)
// 	body.Username = strings.TrimSpace(body.Username)

// 	body.Email = strings.ToLower(body.Email)
// 	body.Username = strings.ToLower(body.Username)

// 	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
// 	defer cancel()

// 	existsUsername, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
// 		Field: "username",
// 		Value: body.Username,
// 	})
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("failed check username uniques", l.Error(err))
// 		return
// 	}
// 	if existsUsername.Exists {
// 		c.JSON(http.StatusBadRequest, gin.H{
// 			"error": err.Error(),
// 			"info":  "please enter another username",
// 		})
// 		h.log.Error("this username already exists", l.Error(err))
// 		return
// 	}

// 	existsEmail, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
// 		Field: "email",
// 		Value: body.Email,
// 	})
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("failed check email uniques", l.Error(err))
// 		return
// 	}
// 	if existsEmail.Exists {
// 		c.JSON(http.StatusBadRequest, gin.H{
// 			"error": err.Error(),
// 			"info":  "please enter another email",
// 		})
// 		h.log.Error("this email already exists", l.Error(err))
// 		return
// 	}
// 	code := etc.GenerateCode(6)
// 	// send to email/phone code
// 	from := "alievazam.200@gmail.com"
// 	password := "azamaluev"

// 	to := body.Email
// 	smtpHost := "smtp.gmail.com"
// 	smtpPort := "587"

// 	message := []byte(code)
// 	auth := smtp.PlainAuth("", from, password, smtpHost)

// 	// err = smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("error while sending verify code to email", l.Error(err))
// 		return
// 	}

// 	body.Code = code
// 	userBodyByte, err := json.Marshal(body)
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("error while marshal user bodyu", l.Error(err))
// 		return
// 	}
// 	err = h.redis.SetWithTTL(body.Email, string(userBodyByte), 300)
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("error set to redis user body", l.Error(err))
// 		return
// 	}
// 	c.JSON(http.StatusAccepted, code)
// }

// // func (h *handlerV1) Verify(c *gin.Context) {
// // 	var (
// // 		code  = c.Param("code")
// // 		email = c.Param("email")
// // 		body  user.User
// // 	)
// // 	userBody, err := h.redis.Get(email)
// // 	if err != nil {
// // 		c.JSON(http.StatusInternalServerError, gin.H{
// // 			"error": err.Error(),
// // 		})
// // 		h.log.Error("error while get from redis user body", l.Error(err))
// // 		return
// // 	}
// // 	byteDate, err := redis.String(userBody)
// // 	if err != nil {
// // 		c.JSON(http.StatusInternalServerError, gin.H{
// // 			"error": err.Error(),
// // 		})
// // 		h.log.Error("error whike get from redis user body", l.Error(err))
// // 		return
// // 	}

// // 	err = json.Unmarshal(&body)

// // }
