package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github/FIrstService/template-service/product-service/api-gateway/api/handlers/models"
	pb "github/FIrstService/template-service/product-service/api-gateway/genproto/product"
	l "github/FIrstService/template-service/product-service/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create Product
// @Summary      Create product
// @Description  Creates new products
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product   body models.ProductFullInfo     true  "Products"
// @Success      200  {object}  models.ProductFullInfoResponse
// @Router       /v1/products [post]
func (h *handlerV1) CreateProduct(c *gin.Context) {
	var (
		body        models.ProductFullInfo
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	p := []*pb.Store{}
	for _, i := range body.Stores {
		a := []*pb.Address{}
		for _, j := range i.Addresses {
			a = append(a, &pb.Address{
				District: j.District,
				Street:   j.Street,
			})
		}
		p = append(p, &pb.Store{
			Name:      i.Name,
			Addresses: a,
		})
	}
	response, err := h.serviceManager.ProductService().CreateProduct(ctx, &pb.ProductFullInfo{
		Name:       body.Name,
		Model:      body.Model,
		TypeId:     int64(body.TypeId),
		CategoryId: int64(body.CategoryId),
		Price:      float32(body.Price),
		Amount:     int64(body.Amount),
		Stores:     p,
	})
	fmt.Println(response, err)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create product", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// Create Category
// @Summary      Create category
// @Description  Creates new category
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product   body models.Category     true  "Category"
// @Success      200  {object}  models.Category
// @Router       /v1/category [post]
func (h *handlerV1) CreateCategory(c *gin.Context) {
	var (
		body        models.Category
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ProductService().CreateCategory(ctx, &pb.Category{
		Name: body.Name,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create category", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// Create Type
// @Summary      Create type
// @Description  Creates new type
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product   body models.Type     true  "Type"
// @Success      200  {object}  models.Type
// @Router       /v1/type [post]
func (h *handlerV1) CreateType(c *gin.Context) {
	var (
		body        models.Type
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ProductService().CreateType(ctx, &pb.Type{
		Name: body.Name,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create category", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// GetProductinfobyId
// @Summary      GetProductinfobyId
// @Description  Get Product info by Id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path int    true  "product_id"
// @Success      200  {object}  models.ProductFullInfoResponse
// @Router       /v1/products/{id} [get]
func (h *handlerV1) GetProductinfobyID(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().GetProductInfoByid(
		ctx, &pb.Ids{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)

}

// UpdateById
// @Summary      UpdateinfobyId
// @Description  Update Product info by Id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product body models.Product    true "Update"
// @Success      200  {object}  models.Product
// @Router       /v1/update/{id} [post]
func (h *handlerV1) UpdateByID(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
		body        models.Product
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().UpdateByid(ctx, &pb.Product{
		Id:    int64(body.Id),
		Name:  body.Name,
		Model: body.Model,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)

}

// DeleteProductinfobyId
// @Summary      DeleteProductinfobyId
// @Description  Delete Product info by Id
// @Tags        product
// @Accept       json
// @Produce      json
// @Param        id path int    true  "product_id"
// @Success      200
// @Router       /v1/delete/{id} [delete]
func (h *handlerV1) DeleteInfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().DeleteInfo(
		ctx, &pb.Ids{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)
}
