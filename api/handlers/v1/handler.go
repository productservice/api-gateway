package v1

import (
	"github/FIrstService/template-service/product-service/api-gateway/config"
	"github/FIrstService/template-service/product-service/api-gateway/pkg/logger"
	"github/FIrstService/template-service/product-service/api-gateway/services"
	"github/FIrstService/template-service/product-service/api-gateway/storage/repo"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
	redis          repo.RedisRepo
}

// HandlerV1Config ...

type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Cfg            config.Config
	Redis          repo.RedisRepo
}

// New ...

func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.Redis,
	}
}
