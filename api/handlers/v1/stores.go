package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"

	"github/FIrstService/template-service/product-service/api-gateway/api/handlers/models"
	pbp "github/FIrstService/template-service/product-service/api-gateway/genproto/stores"
	l "github/FIrstService/template-service/product-service/api-gateway/pkg/logger"
)

// Create Store
// @Summary      Create stores
// @Description  Creates new stores
// @Tags         stores
// @Accept       json
// @Produce      json
// @Param        stores   body models.StoreRequest     true  "Stores"
// @Success      200  {object}  models.StoreResponse
// @Router       /v1/stores [post]
func (h *handlerV1) CreateStore(c *gin.Context) {
	var (
		body        models.StoreRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	s := []*pbp.Address{}
	for _, i := range body.Address {
		s = append(s, &pbp.Address{
			District: i.District,
			Street:   i.Street,
		})
	}

	response, err := h.serviceManager.StoreService().CreateStore(ctx, &pbp.StoreRequest{
		Name:    body.Name,
		Address: s,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create store", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// GetStoreinfobyId
// @Summary      GetStoreinfobyId
// @Description  Get Store info by Id
// @Tags         stores
// @Accept       json
// @Produce      json
// @Param        id path int    true  "store_id"
// @Success      200  {object}  models.StoreResponse
// @Router       /v1/stores/{id} [get]
func (h *handlerV1) GetStore(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.StoreService().GetStore(
		ctx, &pbp.GetstoreInfobyid{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get store")
		return
	}
	c.JSON(http.StatusOK, response)

}
