package services

import (
	"fmt"

	"github/FIrstService/template-service/product-service/api-gateway/config"
	pb "github/FIrstService/template-service/product-service/api-gateway/genproto/product"
	pbp "github/FIrstService/template-service/product-service/api-gateway/genproto/stores"
	u "github/FIrstService/template-service/product-service/api-gateway/genproto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

type IServiceManager interface {
	ProductService() pb.ProductServiceClient
	StoreService() pbp.StoreServiceClient
	UserService() u.UserServiceClient
}

type serviceManager struct {
	productService pb.ProductServiceClient
	storeService   pbp.StoreServiceClient
	userService    u.UserServiceClient
}

func (s *serviceManager) ProductService() pb.ProductServiceClient {
	return s.productService
}

func (s *serviceManager) StoreService() pbp.StoreServiceClient {
	return s.storeService
}

func (s *serviceManager) UserService() u.UserServiceClient {
	return s.userService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connProduct, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.ProductServiceHost, conf.ProductServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connStore, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.StoreServiceHost, conf.StoreServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		productService: pb.NewProductServiceClient(connProduct),
		storeService:   pbp.NewStoreServiceClient(connStore),
		userService:    u.NewUserServiceClient(connUser),
	}

	return serviceManager, nil
}
